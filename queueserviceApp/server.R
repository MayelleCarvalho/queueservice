#------------------------- BIBLIOTECAS:
library(shiny)

#----------- CÓDIGO SERVER:

function(input, output, session) {
  
    ## gráfico da simulação
  
  output$grafico <- renderPlot({
    set.seed(08072019)
    N = 1000
    
    lambda <- input$cliente/input$tempo_total_atend 
    
    intervalo_chegada <- 1 / lambda
    
    tempo_atendimento <- input$atendente/input$tempo_medio_atend
    
  
    chegadas_simuladas <- rexp(N, rate = intervalo_chegada)
    
    chegadas_simuladas_acumulada <- cumsum(chegadas_simuladas)
    chegadas_simuladas_acumulada <- chegadas_simuladas_acumulada[chegadas_simuladas_acumulada <= input$tempo_total_atend]
    
    num_clientes <- length(chegadas_simuladas_acumulada)
    
    atendimentos_simulados <- rexp(n = num_clientes, rate = tempo_atendimento)
    
    atendimentos_simulados_acumulado <- cumsum(atendimentos_simulados)
    
    tempo_espera <- atendimentos_simulados_acumulado - chegadas_simuladas_acumulada
    
    plot(1:num_clientes,
         tempo_espera,
         type ="l",
         xlab = "Cliente",
         ylab = "Tempo de espera (minutos)",
         main = "Tempo de espera por cliente")
  })
  
  # tabela de dados da simulação
  
  inputValues <- reactive({
    
    tx_chegada <- input$cliente/input$tempo_total_atend
    intv_chegada <- 1/(input$cliente/input$tempo_total_atend)
    tx_atendimento <- input$atendente/input$tempo_medio_atend
    comprimento_fila <- (tx_chegada * tx_chegada)/(tx_atendimento*(tx_atendimento - tx_chegada))
    tp_espera <- comprimento_fila/tx_chegada
    prob <- tx_chegada/tx_atendimento
    prob_espera <- (1-(1 - prob)) * 100
    
    data.frame(
      Variaveis = c(
               "Quantidade de Atendentes",
               "Quantidade de Clientes",
               "Tempo de atendimento",
               "Ritmo de chegada",
               "Intervalo médio entre chegada",
               "Tempo médio de atendimento",
               "Taxa de atendimento",
               "Taxa média de Espera",
               "Probabilidade de Esperar"
               ),
      Valores = as.character(c(
                             input$atendente,
                             input$cliente,
                             input$tempo_total_atend,
                             format(tx_chegada, digits = 3),
                             format(intv_chegada, digits = 3),
                             format(input$tempo_medio_atend, digits = 3),
                             format(tx_atendimento, digits = 3),
                             format(tp_espera, digits = 3),
                             format(prob_espera, digits = 2)
                           )
                         ),
      Unidade = c(
                  "Atendentes",
                  "Clientes",
                  "Minutos",
                  "Clientes/min",
                  "Minutos",
                  "Minutos",
                  "Clientes/min",
                  "Minutos",
                  "%"
      ),
      Descricao = c(
                  "Quantidade de atendentes utilizadas no serviço.",
                  "Quantidade de Clientes Atendidos durante o tempo de expediente.",
                  "Tempo total em minutos de intervalo de expediente de atendimento.",
                  "Taxa de chegadas de cliente a cada unidade de tempo (1 minuto).",
                  "Intervalo médio de tempo que leva para chegar 1 cliente.",
                  "Tempo médio para um atendente atender a 1 cliente.",
                  "Quantidade de cliente que pode ser atendido em média por cada unidade de tempo (1 minuto).",
                  "Quantidade de tempo médio que um cliente espera para ser atendido.",
                  "A probilidade de um cliente ter que esperar para um atendimento em porcentagem"
                  ),
      stringsAsFactors = FALSE)
    
  })

  output$table <- renderTable({
    inputValues()
  })
  
  
  ### extra:
  
  #análise da base de dados de fila de atendimento hospital universitário:
  output$tab_fila_clinica <- renderTable(
    data.frame(
      Tempo = c(base_clinica$CHEGADA_1)
    )
  )
  
  output$table_dados_clinica <- renderTable({
    
    comprimento_fila_clinica <- (lambda_clinica * lambda_clinica)/(taxa_atendimento_clinica*(taxa_atendimento_clinica - lambda_clinica))
    taxa_espera_clinica <- comprimento_fila_clinica/lambda_clinica
    prob_clinica <- lambda_clinica/taxa_atendimento_clinica
    prob_esperar_clinica <- (1-(1 - prob_clinica)) * 100
    
    data.frame(
      Variaveis = c(
        "Taxa de chegada de clientes (clientes/minuto)",
        "Intervalo de chegada entre clientes (minutos)",
        "Tempo médio de atendimento (minutos)",
        "Taxa de atendimento cliente (cliente/minuto)",
        "Comprimento médio da fila (clientes)",
        "Taxa média de Espera (minutos)",
        "Probabilidade de Esperar (%)"
      ),
      Valores = c(
        format(lambda_clinica, digits = 2),
        format(intervalo_chegada_clinica, digits = 2),
        format(mi_clinica, digits = 2),
        format(taxa_atendimento_clinica, digits = 2),
        format(comprimento_fila_clinica, digits = 2),
        format(taxa_espera_clinica, digits = 2),
        format(prob_esperar_clinica, digits = 2)
      )
    )
  })
  
  output$tab_fila_clinica <- DT::renderDataTable(
    DT::datatable(base_bruta, options = list(pageLength = 20))
  )
  
  output$grafico_clinica <- renderPlot({
    
    set.seed(09072019)
    
    N = input$simulacao_clinica
 
    chegadas_simulada_clinica <- rexp(N, rate = intervalo_chegada_clinica)
    
    chegadas_simuladas_clinica_acumulada <- cumsum(chegadas_simulada_clinica)
    chegadas_simuladas_clinica_acumulada <- chegadas_simuladas_clinica_acumulada[chegadas_simuladas_clinica_acumulada <= 180]
    
    num_clientes_clinica <- length(chegadas_simuladas_clinica_acumulada)
    
    atendimentos_simulado_clinica <- rexp(n = num_clientes_clinica, rate = mi_clinica)
    
    atendimentos_simulados_clinica_acumulado <- cumsum(atendimentos_simulado_clinica)
    
    tempo_espera_clinica <- atendimentos_simulados_clinica_acumulado - chegadas_simuladas_clinica_acumulada
    
    plot(1:num_clientes_clinica,
         tempo_espera_clinica,
         type ="l",
         xlab = "Clientes",
         ylab = "Tempo de espera (minutos)",
         main = "Tempo de espera por cliente")
  })
  
}



  
